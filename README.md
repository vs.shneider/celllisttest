#Install
`
pip install selenium
`

`
pip install pytest
`

`
pip install pytest-xdist
`

#chromedriver
`
https://chromedriver.chromium.org/downloads
`
download the needed file for your browser and put it in the root of the project 

#Run
`
pytest -n 3 --dist loadgroup -v -m my_test
`