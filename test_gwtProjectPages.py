import pytest

from gwtProjectPages import SearchHelper


# test 1 - adding a completed contact
# test 2 - adding a partially filled contact
# test 3 - adding a contact with an empty Birthday field


@pytest.mark.parametrize("first_name, last_name, category, birthday, address, state_before, state_after", [
    ('some_first_name', 'some_last_name', 'Coworkers', '15', 'any_address', '0 - 30 : 250', '0 - 30 : 251'),
    ('', '', 'Family', '15', '', '0 - 30 : 250', '0 - 30 : 251'),
    ('some_first_name', 'some_last_name', 'Businesses', None, 'any_address', '0 - 30 : 250', '0 - 30 : 250')
])
@pytest.mark.my_test
@pytest.mark.xdist_group(name="create")
def test_create_new_contact(browser, first_name, last_name, category, birthday, address, state_before, state_after):
    gwt_project_page = SearchHelper(browser)
    gwt_project_page.go_to_site()

    gwt_project_page.enter_first_name(first_name)
    gwt_project_page.enter_last_name(last_name)
    gwt_project_page.enter_category(category)
    gwt_project_page.enter_birthday(birthday)
    gwt_project_page.enter_address(address)

    check_state_before = gwt_project_page.check_legend(state_before)
    gwt_project_page.click_on_the_create_contact_button()
    check_state_after = gwt_project_page.check_legend(state_after)

    assert check_state_before
    assert check_state_after


# test 4 - adding 50 contacts


@pytest.mark.my_test
@pytest.mark.xdist_group(name="create_50")
def test_create_50_new_random_contact(browser):
    gwt_project_page = SearchHelper(browser)
    gwt_project_page.go_to_site()

    check_state_before = gwt_project_page.check_legend('0 - 30 : 250')
    gwt_project_page.click_on_the_create_50_contact_button()
    check_state_after = gwt_project_page.check_legend('0 - 30 : 300')

    assert check_state_before
    assert check_state_after


# test 5 - checking the contact selection (field 'address')


@pytest.mark.my_test
@pytest.mark.xdist_group(name="select_contact")
def test_select_contact_5(browser):
    gwt_project_page = SearchHelper(browser)
    gwt_project_page.go_to_site()

    line_5_address = gwt_project_page.get_line_5_address()
    gwt_project_page.enter_line_5()
    check_line_5_address = gwt_project_page.compare_address_5(line_5_address)

    assert check_line_5_address
