from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.wait import WebDriverWait


class BasePage:

    def __init__(self, driver):
        self.driver = driver
        self.url = "http://samples.gwtproject.org/samples/Showcase/Showcase.html#!CwCellList"

    def text_to_be_present_in_element_value(self, locator, text, time=10):
        return WebDriverWait(self.driver, time).until(EC.text_to_be_present_in_element_value(locator, text),
                                                      message=f"Can't find element by locator {locator}")

    def text_to_be_present_in_element(self, locator, text, time=10):
        return WebDriverWait(self.driver, time).until(EC.text_to_be_present_in_element(locator, text),
                                                      message=f"Can't find element by locator {locator}")

    def presence_of_element_located(self, locator, time=10):
        return WebDriverWait(self.driver, time).until(EC.presence_of_element_located(locator),
                                                      message=f"Can't find element by locator {locator}")

    def presence_of_all_elements_located(self, locator, time=10):
        return WebDriverWait(self.driver, time).until(EC.presence_of_all_elements_located(locator),
                                                      message=f"Can't find elements by locator {locator}")

    def select_drop_down(self, locator):
        return Select(self.presence_of_element_located(locator))

    def go_to_site(self):
        return self.driver.get(self.url)
