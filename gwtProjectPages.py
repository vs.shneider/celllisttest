from selenium.webdriver.common.by import By

from BaseApp import BasePage


class GwtProjectLocators:
    FIRST_NAME_LOCATOR = (By.XPATH,
                          '//*[@id="gwt-debug-contentPanel"]/div[2]/div/div[2]/div/div[3]/div/div/div/table/tbody/tr[1]/td[2]/table/tbody/tr[2]/td[2]/div/div/table/tbody/tr[2]/td[2]/input')
    LAST_NAME_LOCATOR = (By.XPATH,
                         '//*[@id="gwt-debug-contentPanel"]/div[2]/div/div[2]/div/div[3]/div/div/div/table/tbody/tr[1]/td[2]/table/tbody/tr[2]/td[2]/div/div/table/tbody/tr[3]/td[2]/input')
    CATEGORY_LOCATOR = (By.XPATH,
                        '//*[@id="gwt-debug-contentPanel"]/div[2]/div/div[2]/div/div[3]/div/div/div/table/tbody/tr[1]/td[2]/table/tbody/tr[2]/td[2]/div/div/table/tbody/tr[4]/td[2]/select')
    ADDRESS_LOCATOR = (By.XPATH,
                       '//*[@id="gwt-debug-contentPanel"]/div[2]/div/div[2]/div/div[3]/div/div/div/table/tbody/tr[1]/td[2]/table/tbody/tr[2]/td[2]/div/div/table/tbody/tr[6]/td[2]/textarea')
    BIRTHDAY_LOCATOR = (By.XPATH,
                        '//*[@id="gwt-debug-contentPanel"]/div[2]/div/div[2]/div/div[3]/div/div/div/table/tbody/tr[1]/td[2]/table/tbody/tr[2]/td[2]/div/div/table/tbody/tr[5]/td[2]/input')
    LIST_OF_THE_DAYS_LOCATOR = (By.XPATH, '/html/body/div[5]/div/table/tbody/tr[2]/td/table/tbody/tr/td')

    CREATE_CONTACT_BUTTON_LOCATOR = (By.XPATH,
                                     '//*[@id="gwt-debug-contentPanel"]/div[2]/div/div[2]/div/div[3]/div/div/div/table/tbody/tr[1]/td[2]/table/tbody/tr[2]/td[2]/div/div/table/tbody/tr[7]/td/button[2]')
    CREATE_50_CONTACT_BUTTON_LOCATOR = (By.XPATH,
                                        '//*[@id="gwt-debug-contentPanel"]/div[2]/div/div[2]/div/div[3]/div/div/div/table/tbody/tr[1]/td[2]/button')

    LEGEND = (By.XPATH,
              '//*[@id="gwt-debug-contentPanel"]/div[2]/div/div[2]/div/div[3]/div/div/div/table/tbody/tr[1]/td[1]/div[2]')

    LIST_ITEM_5 = (By.XPATH,
                   '//*[@id="gwt-debug-contentPanel"]/div[2]/div/div[2]/div/div[3]/div/div/div/table/tbody/tr[1]/td[1]/div[1]/div/div/div[1]/div[5]')
    LIST_ITEM_5_ADDRESS = (By.XPATH,
                           '//*[@id="gwt-debug-contentPanel"]/div[2]/div/div[2]/div/div[3]/div/div/div/table/tbody/tr[1]/td[1]/div[1]/div/div/div[1]/div[5]/table/tbody/tr[2]/td')


class SearchHelper(BasePage):

    def __init__(self, driver):
        super().__init__(driver)

    def enter_first_name(self, value):

        search_field = self.presence_of_element_located(GwtProjectLocators.FIRST_NAME_LOCATOR)
        search_field.click()
        search_field.send_keys(value)

    def enter_last_name(self, value):
        search_field = self.presence_of_element_located(GwtProjectLocators.LAST_NAME_LOCATOR)
        search_field.click()
        search_field.send_keys(value)

    def enter_category(self, value):
        search_field = self.select_drop_down(GwtProjectLocators.CATEGORY_LOCATOR)
        search_field.select_by_visible_text(value)

    def enter_birthday(self, value):
        if value is None or value == '':
            return None

        search_field = self.presence_of_element_located(GwtProjectLocators.BIRTHDAY_LOCATOR)
        search_field.click()

        m = self.presence_of_all_elements_located(GwtProjectLocators.LIST_OF_THE_DAYS_LOCATOR)
        for i in m:
            if i.text == value:
                i.click()
                break

    def enter_address(self, value):
        search_field = self.presence_of_element_located(GwtProjectLocators.ADDRESS_LOCATOR)
        search_field.click()
        search_field.send_keys(value)

    def click_on_the_create_contact_button(self):
        return self.presence_of_element_located(GwtProjectLocators.CREATE_CONTACT_BUTTON_LOCATOR).click()

    def click_on_the_create_50_contact_button(self):
        return self.presence_of_element_located(GwtProjectLocators.CREATE_50_CONTACT_BUTTON_LOCATOR).click()

    def check_legend(self, text):
        return self.text_to_be_present_in_element(GwtProjectLocators.LEGEND, text)

    def enter_line_5(self):
        search_field = self.presence_of_element_located(GwtProjectLocators.LIST_ITEM_5)
        search_field.click()
        return search_field

    def get_line_5_address(self):
        return self.presence_of_element_located(GwtProjectLocators.LIST_ITEM_5_ADDRESS).text

    def compare_address_5(self, text):
        return self.text_to_be_present_in_element_value(GwtProjectLocators.ADDRESS_LOCATOR, text)
